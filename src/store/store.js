import { Store } from 'vuex';

import cat from './modules/cat';
import http from './modules/http';
import pagination from './modules/pagination';

const store = new Store({
  modules: {
    cat,
    http,
    pagination,
  },
});

export default store;
