import CatBreedsService from '../../services/CatBreedsService';

export default {
  namespaced: true,
  state: {
    breeds: undefined,
    totalBreeds: 0,
  },
  mutations: {
    SET_BREEDS(state, breeds) {
      state.breeds = breeds;
    },
    SET_TOTAL_BREEDS(state, totalBreeds) {
      state.totalBreeds = totalBreeds;
    },
  },
  actions: {
    async fetchBreeds({ commit, rootState }) {
      let breeds = [];
      let paginationCount = 0;
      try {
        const response = await CatBreedsService.getBreeds(
          rootState.pagination.page,
        );

        breeds = response.data;
        paginationCount = response.headers['pagination-count'];
      } catch (error) {
        console.error(error);
      }

      commit('SET_BREEDS', breeds);
      commit('SET_TOTAL_BREEDS', paginationCount);
    },
    async fetchBreedsByName({ commit, dispatch }, name) {
      if (name === '') {
        dispatch('fetchBreeds');
        return;
      }

      let breeds = [];
      let paginationCount = 0;

      try {
        const response = await CatBreedsService.getBreedsByName(name);
        breeds = response.data;
        paginationCount = response.headers['pagination-count']
          ? response.headers['pagination-count']
          : response.data.length
          ? response.data.length
          : 1;
      } catch (error) {
        console.error(error);
      }

      commit('SET_BREEDS', breeds);
      commit('SET_TOTAL_BREEDS', paginationCount);
    },
  },
  getters: {},
};
