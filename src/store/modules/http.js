export default {
  namespaced: true,
  state: {
    reqInProgress: false,
  },
  mutations: {
    SET_REQUEST_IN_PROGRESS(state, status) {
      state.reqInProgress = status;
    },
  },
  actions: {
    httpReqStart({ commit }) {
      commit('SET_REQUEST_IN_PROGRESS', true);
    },
    httpReqEnd({ commit }) {
      commit('SET_REQUEST_IN_PROGRESS', false);
    },
  },
  getters: {},
};
