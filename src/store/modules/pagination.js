export default {
  namespaced: true,
  state: {
    page: 0,
  },
  mutations: {
    GOTO_PREVIOUS_PAGE(state) {
      state.page -= 1;
    },
    GOTO_NEXT_PAGE(state) {
      state.page += 1;
    },
    GOTO_PAGE(state, page) {
      state.page = page;
    },
  },
  actions: {
    goToPreviousPage({ commit, dispatch }) {
      commit('GOTO_PREVIOUS_PAGE');
      dispatch('cat/fetchBreeds', null, { root: true });
    },
    goToNextPage({ commit, dispatch }) {
      commit('GOTO_NEXT_PAGE');
      dispatch('cat/fetchBreeds', null, { root: true });
    },
    goToPage({ commit, dispatch }, page) {
      commit('GOTO_PAGE', page);
      dispatch('cat/fetchBreeds', null, { root: true });
    },
  },
  getters: {
    totalPages(_, __, rootState) {
      return Math.ceil(
        rootState.cat.totalBreeds / import.meta.env.VITE_PAGINATION_LIMIT,
      );
    },
  },
};
