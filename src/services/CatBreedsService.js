import axios from 'axios';
import store from '../store/store';

const axiosClient = axios.create({
  baseURL: `${import.meta.env.VITE_THECATAPI_BASEURL}`,
  headers: {
    'x-api-key': `${import.meta.env.VITE_THECATAPI_TOKEN}`,
  },
});

axiosClient.interceptors.request.use(
  function (config) {
    store.dispatch('http/httpReqStart');
    return config;
  },
  function (error) {
    store.dispatch('http/httpReqEnd');
    return Promise.reject(error);
  },
);

axiosClient.interceptors.response.use(
  function (response) {
    store.dispatch('http/httpReqEnd');
    return response;
  },
  function (error) {
    store.dispatch('http/httpReqEnd');
    return Promise.reject(error);
  },
);

export default {
  getBreeds(page = 0, limit = import.meta.env.VITE_PAGINATION_LIMIT) {
    const params = {};
    if (page) {
      params['page'] = page;
    }
    if (limit) {
      params['limit'] = limit;
    }
    return axiosClient.get('breeds', {
      params: params,
    });
  },
  getBreedsByName(name) {
    return axiosClient.get('breeds/search', {
      params: {
        q: name,
      },
    });
  },
  getImageByImageId(id) {
    return axiosClient.get(`images/${id}`);
  },
};
